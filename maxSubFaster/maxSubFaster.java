import java.util.Scanner;

public class maxSubFaster {

    public static int subFaster(int[] A){ //Generate subFaster method
        int S[] = new int[A.length]; 
        S[0] = 0; //set S array index 0 = 0
        int m ; //keep max sum
        int s = 0; //keep result sum of sub array
        
        for(int i=1; i< A.length; i++){ 
            S[i] = S[i-1] + A[i] ; //step to do subArray
        }
        m = 0; //set value max is 0
        for(int j = 1; j< A.length ; j++){ 
            for(int k=j; k< A.length; k++){
                s = S[k] - S[j-1]; //loop for finding sum of all subarray
                if( s > m ){ //comparison between sum and max
                    m = s ; //if true then max == sum
                }
            }
        }

        return m ; 

    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(new InputStreamReader(System.in));
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < n; i++) { //loop for add input into array
            A[i] = kb.nextInt();
        }

        int m = subFaster(A) ;
        System.out.println(m);
    }
}
