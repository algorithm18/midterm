import java.util.Scanner;

class reversAlgor {

    public static int[] reversArray(int[] A) { //Generate reversArray() method
        
        for(int i = 0; i < A.length/2 ; i++){
            int temp = A[A.length - 1 - i];
            A[A.length - 1 -i] = A[i];
            A[i] = temp;
        } 
        for(int i=0 ; i< A.length ; i++){
            System.out.print(A[i] + " ");
        }
        return A;
    }


    public static void main(String[] args) {
        Scanner kb = new Scanner(new InputStreamReader(System.in));
        int n = kb.nextInt();
        int[] A = new int[n];
        for (int i = 0; i < n; i++) {
            A[i] = kb.nextInt();
        }

        reversArray(A);
    }    
}
